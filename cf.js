const { casesData } = require('./data');

function calculateCertaintyFactor(positiveFactor, negativeFactor) {
  return positiveFactor - negativeFactor * (1 - positiveFactor);
}

function performDiagnosis(inputSymptoms) {
  let maxCertaintyFactor = -1;
  let selectedDiagnosis = null;
  let confidence = 0;

  casesData.forEach(diagnosis => {
    const { symptomIds, positiveFactor, negativeFactor } = diagnosis;
    const matchedSymptoms = inputSymptoms.filter(inputSymptom => symptomIds.includes(inputSymptom.id));

    const certaintyFactor = calculateCertaintyFactor(positiveFactor, negativeFactor);

    if (matchedSymptoms.length > 0 && certaintyFactor > maxCertaintyFactor) {
      maxCertaintyFactor = certaintyFactor;
      selectedDiagnosis = diagnosis.diagnosis;
      confidence = Math.round(certaintyFactor * 100);
    }
  });

  return { diagnosis: selectedDiagnosis, confidence };
}

module.exports = {
  performDiagnosis
};
