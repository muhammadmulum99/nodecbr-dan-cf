const { symptomsData, casesData } = require('./data');

function findMatchingCase(inputSymptoms) {
  return casesData.find((caseData) => {
    const caseSymptomIds = caseData.symptomIds;
    const inputSymptomIds = inputSymptoms.map(symptom => symptom.id);
    return JSON.stringify(caseSymptomIds.sort()) === JSON.stringify(inputSymptomIds.sort());
  });
}

module.exports = {
  findMatchingCase
};
