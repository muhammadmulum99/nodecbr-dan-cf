const express = require('express');
const bodyParser = require('body-parser');
const { findMatchingCase } = require('./cbr');
const { performDiagnosis } = require('./cf');

const app = express();
const PORT = 3000;

app.use(bodyParser.json());

app.post('/diagnosa', (req, res) => {
  const inputSymptoms = req.body.symptoms;

  // Diagnosa menggunakan metode CBR
  const cbrDiagnosis = findMatchingCase(inputSymptoms);

  // Diagnosa menggunakan metode CF
  const cfDiagnosis = performDiagnosis(inputSymptoms);

  res.json({
    cbrDiagnosis: cbrDiagnosis ? cbrDiagnosis.diagnosis : 'Tidak ada diagnosa (CBR)',
    cfDiagnosis: cfDiagnosis.diagnosis || 'Tidak ada diagnosa (CF)',
    confidence: cfDiagnosis.confidence || 0
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
